/* GROUP MEMBERS
Jakub Pakula
Leona McCahill
Krzysztof Baran
Xi Zhang
Leon Oska */

import java.util.Scanner;
import java.util.ArrayList;

public class keyBoardPhone {
    public static int[] NumtoArray(int number) {
        int len = Integer.toString(number).length();

        int [] digits = new int [len];

        for(int i = len-1; i > -1; i--) {
            digits[i] = number % 10;
            number /= 10;
        }
    return digits;
    }


    public static void main(String [] args) {
    // Create the keyboard array:
    String [] keyboard = {"0", "1", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"}; 

    //Take in our input convert the inpout of numbers into a string
    Scanner in = new Scanner(System.in);

    int num = in.nextInt();
    
    int [] numbers = NumtoArray(num);

    int n = numbers.length;

    //Initializing list with letters from first digit

    int i = 0;

    int digit = 0;

    ArrayList<String> combinations = new ArrayList<String>();

    while(i < keyboard[numbers[digit]].length()) {
        int first_digit = numbers[digit];
        String letters = keyboard[first_digit];
        combinations.add(Character.toString(letters.charAt(i)));
        i++;
    }


    //Code to update the current list
    //Will repeat for all digits

    digit = 1;

    while(digit < n){
        int comb_len = combinations.size();
        int m = 0;
        //temporary list that allows to delete combinations
        //from past iterations
        ArrayList <String> combinations_temp = new ArrayList<String>();

        while (m < comb_len) {
            int k = 0;
            while (k < keyboard[numbers[digit]].length()) {
                //Concatenate current combination with each possible letter
                String c1 = combinations.get(m);
                String c2 = Character.toString(keyboard[numbers[digit]].charAt(k));
                combinations_temp.add(c1 + c2);
                k ++;
                }
            m ++;
            }
        combinations = combinations_temp;
        digit ++;
        }
    
        System.out.println(combinations);
    }

}
