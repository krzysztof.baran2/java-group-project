// Version by Xi Zhang
// Error Fixing/Testing/Exception Handling by Leon Oska

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Random;

public class CreativeElement
{
    static final String codes[] = {"0", "1", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};

    public static ArrayList<String> findCombinations(String strings)
    {
        if (strings.length() == 0)
        {
            ArrayList<String> base = new ArrayList<>();
            base.add("");
            return base;
        }

        char firstChar = strings.charAt(0);
        String rest = strings.substring(1);

        ArrayList<String> previous = findCombinations(rest);
        ArrayList<String> restChar = new ArrayList<>();

        String code = codes[firstChar - '0'];

        for (String c : previous)
        {
            for (int i = 0; i < code.length(); i++)
            {
                restChar.add(code.charAt(i) + c);
            }
        }
        return restChar;
    }

    public static void creativeFunction()
    {
        Scanner in = new Scanner(System.in);
        Random rand = new Random();
        ArrayList<String> words = new ArrayList<String>();
        words.add("shape");
        words.add("soothe");
        words.add("ford");
        words.add("head");
        words.add("spotted");
        words.add("needless");
        words.add("abundant");
        words.add("humdrum");
        words.add("mouth");
        words.add("time");
        words.add("offbeat");
        words.add("shallow");
        words.add("optimal");
        words.add("yummy");
        words.add("permit");
        words.add("stove");
        words.add("push");
        words.add("theory");
        words.add("mind");
        words.add("crate");
        words.add("man");
        words.add("crime");
        words.add("rinse");
        words.add("company");
        words.add("tricky");
        words.add("cactus");
        words.add("square");
        words.add("innate");
        words.add("piquant");
        words.add("cushion");
        words.add("degree");
        words.add("dress");
        words.add("ready");
        words.add("crown");
        words.add("science");
        words.add("helpful");
        words.add("field");

        long sum = 0;
        long sum_correct = 0;
        String name;

        System.out.println("What's your name?");
        name = in.nextLine();
        System.out.println("Hi " + name + "!");
        System.out.println();
        System.out.println("The following program will prompt you to type 10 words by typing the");
        System.out.println("corresponding number on the keypad once (eg. dog - 364).");
        System.out.println();
        System.out.println("At the end of the test, your typing speed and accuracy will be evaluated.");
        System.out.println();
        System.out.println("The program will load in just a moment.");

        try
        {
            Thread.sleep(8000);
        }
        catch(InterruptedException ex){}

        for (int i = 0; i < 10; i++)
        {
            int random = rand.nextInt(words.size());
            long start = System.currentTimeMillis();
            System.out.print("Please enter the number on the keyboard that spells \"" + words.get(random) + "\": ");

            String str = in.next();
            try
            {
                //System.out.println(Integer.parseInt(x));
                long end = System.currentTimeMillis();

                ArrayList<String> answer = findCombinations(str);

                boolean check = true;

                for (String val : answer) {
                    if (val.equals(words.get(random))) {
                        System.out.println("Correct!");
                        sum_correct += 1;
                        check = false;
                    }
                }

                if (check) {
                    System.out.println("Incorrect");
                }
                sum += (end - start);
            }
            catch (final NumberFormatException e)
            {
                System.out.println("Please enter a valid integer.");
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                System.out.println("Please enter a valid integer.");
            }
        }
        System.out.println("It took you " + sum / 1000 + " seconds to finish!");
        System.out.println("Accuracy = " + sum_correct * 10 + "%");
        System.out.println("Average spelling speed = " + (double) sum / 10000 + "s");
    }

    public static void main (String[] args)
    {
        creativeFunction();
    }
}
