# java-group-project

* This is a JAVA group project submitted for assessment in module "Computer Programming 4(Object-Oriented Programming)"
* The function takes phone number digits as input and outputs all possible letter permutations (phone keyboard).
* I created the logic in the Python file which was then adapted into JAVA.
* A creative element is also included.
* This was a group project so credit goes to respective colleagues:

## Main File Workload
1. Outline/Skeleton/Testing - Jakub Pakula
2. Permutations - Krzysztof Baran & Leona McCahill


# Creative Element
1. Main code - Xi Zhang
2. Exception handling/debugging/output script - Leon Oska


You may access the project elements below:

* [Main Programme](keyBoardPhone.java)
* [Creative Element](CreativeElement.java)