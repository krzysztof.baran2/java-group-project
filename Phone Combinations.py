#Phone combinations - Python Version - Krzysztof Baran

#Mapping
d = {
"2": "abc",
"3": "def",
"4": "ghi",
"5": "jkl",
"6": "mno",
"7": "pqrs",
"8": "tuv",
"9": "wxyz"}

#Test
input = "234567"
# Memory limit? - can't paste full answer if len(input) > 6
n = len(input)

#Initializing list with letters from first digit
i = 0
digit = 0
combinations = []
while i < len(d[input[digit]]):
    combinations.append(d[input[digit]][i])
    i = i + 1

# Code to update the current list
# Will repeat for all digits
digit = 1
while digit < n:
    comb_len = len(combinations)
    m = 0
    # temporary list that allows to delete combinations from past iterations and keep only the newest one
    combinations_temp = []
    while m < comb_len:
        k = 0
        while k < len(d[input[digit]]):
            # Concatenate current combination with each possible letter
            combinations_temp.append(combinations[m] + d[input[digit]][k])
            k = k + 1
        m = m + 1
    combinations = combinations_temp
    digit = digit + 1
print(combinations)